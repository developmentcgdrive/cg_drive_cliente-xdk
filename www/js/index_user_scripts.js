/*jshint browser:true */
/*global $ */(function()
{
 "use strict";
 /*
   hook up event handlers 
 */
 function register_event_handlers()
 {
     
    
    $(document).on("click", "#btnentrar", function(evt)
    {
        var us = $("#txtusers").val();
        if($("#txtusers").val()=== null || $("#txtusers").val() ===""){
        $('#msglog').empty();
        $('#msglog').append('Informe o Email'); 
        return false;
        }
        var sen = $("#txtsenhas").val();
        if($("#txtsenhas").val()=== null || $("#txtsenhas").val() ===""){
        $('#msglog').empty();
        $('#msglog').append('<h4>Informe a senha</h4>'); 
        return false;
        }
         /*global activate_page */
        login(us,sen);
         return false;
    });
     
    $(document).on("click", "#ss", function(evt)
    {
        
      
    var us = $("#txtusers").val();
    var sen = $("#txtsenhas").val();
        
       
    localStorage.setItem("nome",us);
    localStorage.setItem("senha",sen);   
    alert("Senha salva com sucesso."); 
    return false;
        
    });
    
    $(document).on("click", "#btnenviar", function(evt)
    {
       $('#msgcadastro').empty();
       $('#msgcadastro').append('....Analisando...'); 

        var us = $("#txtname").val();
        var pass = $("#txtsenh").val();
        var senc = $("#txtconfsen").val();
        var mail = $("#txtemail").val();
        var fone = $("#txtfone").val();
        var indica = $("#txtindica").val();
         var cpf = $("#txtcpf").val();
        var ret = validateEmail(mail);
        
        if(us === null || us === ""){
        $('#msgcadastro').empty();
        $('#msgcadastro').append('<h4>Informe um Nome</h4>'); 
        return false;
        }
        if(!ret){
        $('#msgcadastro').empty();
        $('#msgcadastro').append('<h4>Email incorreto</h4>'); 
        return false;
        }
        if($("#txtfone").val()=== null || $("#txtfone").val() ===""){
        $('#msgcadastro').empty();
        $('#msgcadastro').append('<h4> um telefone</h4>'); 
        return false;
        }
        if(pass === null || pass === ""){
        $('#msgcadastro').empty();
        $('#msgcadastro').append('<h4>Informe uma senha</h4>'); 
        return false;
        }
        if(senc == pass){
            
          cadastros(us,mail,pass,fone,indica);
            
            localStorage.setItem("nome",mail);
            localStorage.setItem("senha",senc);
        }
        else{
        
        
        $('#msgcadastro').empty();
        $('#msgcadastro').append('<h4>Senha não confere</h4>'); 
        return false;
        }
    });
     $(document).on("click", "#btnenviar1", function(evt)
    {
       $('#msgcadastro1').empty();
       $('#msgcadastro1').append('....ENVIANDO...'); 
         
        var pref = $("#preferencia").val(); 
        var us = $("#1nome").val();
        var email = $("#1email").val();
        var fone = $("#1fone").val();
        var saida = $("#1saida").val();
        var destino = $("#1destino").val();
        var hora = $("#1hora").val();
        var ret = validateEmail(email);
        
        if(us === null || us === ""){
        $('#msgcadastro1').empty();
        $('#msgcadastro1').append('<h4>Informe um Nome</h4>'); 
        return false;
        }
        if(!ret){
        $('#msgcadastro1').empty();
        $('#msgcadastro1').append('<h4>Email incorreto</h4>'); 
        return false;
        }
        if($("#1fone").val()=== null || $("#1fone").val() ===""){
        $('#msgcadastro1').empty();
        $('#msgcadastro1').append('<h4> Informe um telefone</h4>'); 
        return false;
        }
        if(saida === null || saida === ""){
        $('#msgcadastro1').empty();
        $('#msgcadastro1').append('<h4>Informe a saida</h4>'); 
        return false;
        }
        if(destino === null || destino === ""){
        $('#msgcadastro1').empty();
        $('#msgcadastro1').append('<h4>Informe um destino</h4>'); 
        return false;
        }
       
        
          agendar(us,mail,fone,saida,destino,hora);
            
           
        
      
        
    });
     
     $(document).on("click", "#btnenviarm", function(evt)
    {
       $('#msgcadastrom').empty();
       $('#msgcadastrom').append('....Analisando...'); 

        var us = $("#txtnamem").val();
        var mod = $("#txtmodelo").val();
        var ano = $("#txtano").val();
        var mail = $("#txtemailm").val();
        var fone = $("#txtfonem").val();
        
        if($("#txtfonem").val()=== null || $("#txtfonem").val() ===""){
        $('#msgcadastrom').empty();
        $('#msgcadastrom').append('<h4>Informe um telefone</h4>'); 
        return false;
        }
        else{
            
        cadastrosm(us,mail,fone,mod,ano);
        return false;
        }
    });
     $(document).on("click", "#ideDes", function(evt)
    {    
         var dsc = $('#vlrdesc').val();
         $('#lblvlr').empty();
         $('#lblvlr').val(dsc);
         zera();
         $("#Tarifa1").modal("hide");
         $("#atencao").modal("show");
         
         return false;
    });
    $(document).on("click", "#btnlog", function(evt)
    {    
         /*global activate_page */
         activate_page("#login"); 
            $("#txtusers").val(localStorage.getItem('nome'));
         $("#txtsenhas").val(localStorage.getItem('senha'));
         return false;
    });
        /* button  #btncad */
    $(document).on("click", "#btncada", function(evt)
    {
         /*global activate_page */
         activate_page("#cadastro"); 
         return false;
    });
    
    $(document).on("click", "#btnconf", function(evt)
    {
         /*global activate_page */
         ac(); 
         return false;
    });
    
        /* button  #btnvcad */
     $(document).on("click", "#btngps", function(evt)
    {    $("#tipopesq").modal("hide");
         /*global activate_page */
         getMapLocation(); 
         return false;
    });
     $(document).on("click", "#f", function(evt)
    {    $("#avalie").modal("hide");
         /*global activate_page */
         
         return false;
    });
    $(document).on("click", "#att", function(evt)
    {
         /*global activate_page */
         $("#tipopesq").modal("toggle"); 
         return false;
    });
        /* button  #btnvcad */
    $(document).on("click", "#btnvcad", function(evt)
    {
         /*global activate_page */
         activate_page("#mainpage"); 
         return false;
    });
      $(document).on("click", "#btnagenda", function(evt)
    {
         /*global activate_page */
         activate_page("#agenda"); 
         return false;
    });
     $(document).on("click", "#btnvcadm", function(evt)
    {
         /*global activate_page */
         activate_page("#mainpage"); 
         return false;
    });
    $(document).on("click", "#btnvlt", function(evt)
    {
         /*global activate_page */
         activate_page("#mainpage");
         return false;
    });
    
        /* button  #btnchamar */
    
    
        /* button  #btnsair */
    $(document).on("click", "#btnsair", function(evt)
    {
         /*global activate_page */
        activate_page("#login");
         return false;
    });
    
        /* button  .uib_w_8 */
    $(document).on("click", ".uib_w_8", function(evt)
    {
        navigator.app.exitApp();
        return false;
    });
    
        /* button  .uib_w_28 */
    $(document).on("click", ".uib_w_28", function(evt)
    {
        
         $("#SDestino").modal("toggle");  
         return false;
    }); 
    $(document).on("click", "#btnmanual", function(evt)
    {
        
         $("#SSaida").modal("toggle");  
         return false;
    });
        /* button  #btndestino */
    $(document).on("click", "#btnsaida", function(evt)
    {
        var rua;
        var num;
        var bairro;
        var cidade;
        var uf;
        rua = $('#stxtrua').val();
        num = $('#stxtnum').val();
        bairro = $('#stxtbairro').val();
        cidade = $('#stxtcidade').val();
        uf = $('#stxtuf').val();
        
        
        var endereco = rua +","+num+","+bairro+","+cidade+","+uf;
        $("#txtendereco").val(endereco);
        $("#tipopesq").modal("hide");
        $("#SSaida").modal("hide"); 
        carregarNoMapaD(endereco);
        return false;
    });
    $(document).on("click", "#btndestino", function(evt)
    {
        var rua;
        var num;
        var bairro;
        var cidade;
        var uf;
        rua = $('#txtrua').val();
        num = $('#txtnum').val();
        bairro = $('#txtbairro').val();
        cidade = $('#txtcidade').val();
        uf = $('#txtuf').val();
        
        
        var endereco = rua +","+num+","+bairro+","+cidade+","+uf+"";
        $("#SDestino").modal("hide"); 
        carregarNoMapa(endereco);
        return false;
    });
    
        /* button  #btnvalor */
    $(document).on("click", "#btnvalor", function(evt)
    {
         /* Other options: .modal("show")  .modal("hide")  .modal("toggle")
         See full API here: http://getbootstrap.com/javascript/#modals 
            */
        
         $(".uib_w_53").modal("toggle"); 
         return false;
    });
    
        /* button  #btncep */
    $(document).on("click", "#btncep", function(evt)
    {
        var cep;
        cep = $('#txtcep').val();
        pesquisacep(cep);
        return false;
    });
    
        /* button  #btnchama */
    $(document).on("click", "#btnchama", function(evt)
    {
        
         $(".uib_w_53").modal("toggle");  
         return false;
    });
    $(document).on("click", "#btnnao", function(evt)
    {
        
          $("#atencao").modal("hide"); 
         return false;
    });
    $(document).on("click", "#btnc", function(evt)
    {
        
         $("#atencao").modal("toggle");  
         return false;
    });
    $(document).on("click", "#chatdr", function(evt)
    {    chat();
         $("#chatdrive").modal("show");  
         return false;
    });
     $(document).on("click", "#ddo", function(evt)
    {
        reaguarde();
         $("#corrida").modal("show"); 
         
    });
     $(document).on("click", "#ddo2", function(evt)
    {
        aguarde();
         
         $("#corrida").modal("show"); 
         
    });
     $(document).on("click", "#atl", function(evt)
    {
        getMapLocation(); 
         
    });
    $(document).on("click", "#btnchat", function(evt)
    {
        msgc();  
         
    });
        /* button  #btnsoli */
    $(document).on("click", "#btnchamar", function(evt)
       {
         
          
        var  deonde = $("#txtendereco").val(); 
        var paraonde = $("#txtchegada").val();
        var obs = $("#txtobs").val();
        var cliente = $("#idmor").val();
        var tipopg = $("#tipg").val();
        var vlr = $("#lblvlr").val();
        if(deonde === null || deonde=== ""){
        alert("Favor informar a saida,habilte o GPS");
        $("#atencao").modal("hide");
        getMapLocation();
        return false;  
        }
        if(paraonde === null || paraonde === ""){
        alert("Favor  informar o Destino");
        $("#atencao").modal("hide");
        $("#SDestino").modal("toggle");
        return false;
        }
        if(vlr === null || vlr === ""){
        alert("Não possivel calcular o Valor,informe do destino.");
        $("#atencao").modal("hide");
        $("#SDestino").modal("toggle");
        return false;
        }
        else{
        
        chamar(cliente,deonde,paraonde,tipopg,vlr,obs);
         return false;
        } 
    });
    
        /* button  #btnret */
    $(document).on("click", "#btnret", function(evt)
    {
         /*global activate_page */
         activate_page("#Home");
         return false;
    });
     
    $(document).on("click", "#btnavaliar", function(evt)
    {
         var idmot = $("#txtidcor").val();
         
         if(idmot === null || idmot === ""){
        alert("Favor não existe dados de corrida");
         $("#avalia").modal("hide");
         return false;
        }
        else{
         avaliar();
         return false;
        }
    });
    
    $(document).on("click", "#trabalhe", function(evt)
    {
         /*global activate_page */
         activate_page("#cadastromoto"); 
         return false;
    });
     
    $(document).on("click", "#btnperfil", function(evt)
    {
         /*global activate_page */
       $("#chatpage").modal('toggle'); 

         return false;
    });
    $(document).on("click", "#ava", function(evt)
    {
         /*global activate_page */
        $("#corrida").modal("hide");

         $("#avalie").modal('toggle'); 
         return false;
    });
     
    $(document).on("click", "#fechava", function(evt)
    {
         /*global activate_page */
         $("#avalie").modal("hide"); 
         return false;
    });
        /* button  #dds */
    $(document).on("click", "#dds", function(evt)
    {
         /* Other options: .modal("show")  .modal("hide")  .modal("toggle")
         See full API here: http://getbootstrap.com/javascript/#modals 
            */
        reaguarde();
         $("#corrida").modal("toggle");  
         return false;
    });
     $(document).on("click", "#ide", function(evt)
    {
         $("#Tarifa1").modal("hide");
         $("#atencao").modal("show");  
         return false;
    });
     $(document).on("click", "#volte", function(evt)
    {
        
         $("#Tarifa1").modal("hide");  
         return false;
    });
     $(document).on("click", "#lembrar", function(evt)
    {
         /* Other options: .modal("show")  .modal("hide")  .modal("toggle")
         See full API here: http://getbootstrap.com/javascript/#modals 
            */
        
         $("#poplembrar").modal("show");  
         return false;
    });
    $(document).on("click", "#btnlemb", function(evt)
    {
         /* Other options: .modal("show")  .modal("hide")  .modal("toggle")
         See full API here: http://getbootstrap.com/javascript/#modals 
            */
        
         senha();  
         return false;
    });
     $(document).on("click", "#btncanc", function(evt)
    {
         /* Other options: .modal("show")  .modal("hide")  .modal("toggle")
         See full API here: http://getbootstrap.com/javascript/#modals 
            */
         alert("Será cobrada em sua próxima corrida uma taxa de R$ 5,00.");
         cancelar();  
         return false;
    });
   

    }
 document.addEventListener("app.Ready", register_event_handlers, false);
})();
