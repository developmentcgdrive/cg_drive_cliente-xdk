/*
 * Please see the included README.md file for license terms and conditions.
 */


// This file is a suggested starting place for your code.
// It is completely optional and not required.
// Note the reference that includes it in the index.html file.


/*jslint browser:true, devel:true, white:true, vars:true */
/*global $:false, intel:false app:false, dev:false, cordova:false */



// This file contains your event handlers, the center of your application.
// NOTE: see app.initEvents() in init-app.js for event handler initialization code.

// function myEventHandler() {
//     "use strict" ;
// // ...event handler code here...
// }

var Latitude ;
var Longitude ;

// Get geo coordinates

function getMapLocation() {
    
 navigator.geolocation.getCurrentPosition(onMapSuccess, onMapError, { enableHighAccuracy: true });
    
}
function validarcpf(str){
    str = str.replace('.','');
    str = str.replace('.','');
    str = str.replace('-','');
 
    cpf = str;
    var numeros, digitos, soma, i, resultado, digitos_iguais;
    digitos_iguais = 1;
    if (cpf.length < 11)
        return false;
    for (i = 0; i < cpf.length - 1; i++)
        if (cpf.charAt(i) != cpf.charAt(i + 1)){
            digitos_iguais = 0;
            break;
        }
    if (!digitos_iguais){
        numeros = cpf.substring(0,9);
        digitos = cpf.substring(9);
        soma = 0;
        for (i = 10; i > 1; i--)
            soma += numeros.charAt(10 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(0))
            return false;
        numeros = cpf.substring(0,10);
        soma = 0;
        for (i = 11; i > 1; i--)
            soma += numeros.charAt(11 - i) * i;
        resultado = soma % 11 < 2 ? 0 : 11 - soma % 11;
        if (resultado != digitos.charAt(1))
            return false;
        return true;
    }
    else
        return false;
}
// Success callback for get geo coordinates

var onMapSuccess = function (position) {

    Latitude = position.coords.latitude;
    Longitude = position.coords.longitude;
    $("#lat").val(Latitude);
    $("#lng").val(Longitude);
    getMap(Latitude, Longitude);

};

// Get map by using coordinates

function getMap(latitude, longitude) {

    var mapOptions = {
        
        center: new google.maps.LatLng(latitude, longitude),
        zoom: 10,
        mapTypeControl: false,
        streetViewControl: false,
        navigationControl: true,
        scrollwheel: false,
        navigationControlOptions: {style: google.maps.NavigationControlStyle.SMALL},
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map = new google.maps.Map(document.getElementById("map"), mapOptions);


    var latLong = new google.maps.LatLng(latitude, longitude);

    
    var marker = new google.maps.Marker({
    position: latLong,
    icon: "images/cliente.png"
    });

    marker.setMap(map);
    map.setZoom(16);
    map.setCenter(marker.getPosition());
    
    var geocoder = new google.maps.Geocoder();
  
      geocoder.geocode({ // Usando nosso velho amigo geocoder, passamos a latitude e longitude do geolocation, para pegarmos o endereço em formato de string
         "location": new google.maps.LatLng(latitude,longitude)
      },
      function(results, status) {
         if (status == google.maps.GeocoderStatus.OK) {
            $("#txtendereco").val(results[0].formatted_address);
         }
      });
   
    var image2 = "images/makercar2.png"; 
    $.ajax({
                     
                    type: "GET",
                     url:"http://uber.factorysoftwares.com.br/wsdl.php/latlong",
                     timeout: 3000,
                     contentType: "application/json; charset=utf-8",
                     async: false,
                    

                     beforeSend:function() {
                     id= "Enviando Dados ...";
                     },
        
                    success: function (result, jqXHR) {
                    var avisos = JSON.parse(result);
                    $.each(avisos,function(i, aviso){
                   
                            marker = new google.maps.Marker({
                            position: new google.maps.LatLng(aviso.LAT, aviso.LOG),
                            title: aviso.PLACA,
                            map: map,
                            icon: image2,
                            });
                    });
                    },
                           
                           
            error:function(){
            id= "ERRRO";
            },
        });
}

// Success callback for watching your changing position

var onMapWatchSuccess = function (position) {

    var updatedLatitude = position.coords.latitude;
    var updatedLongitude = position.coords.longitude;

    if (updatedLatitude != Latitude && updatedLongitude != Longitude) {

        Latitude = updatedLatitude;
        Longitude = updatedLongitude;

        getMap(updatedLatitude, updatedLongitude);
    }
};

// Error callback

function onMapError(error) {
    console.log('code: ' + error.code + '\n' +
        'message: ' + error.message + '\n');
}

// Watch your changing position

function watchMapPosition() {

    return navigator.geolocation.watchPosition(onMapWatchSuccess, onMapError, { enableHighAccuracy: true });
}


function getGeoLocation(){
    var success = function(position){
        map.panTo(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
    }
    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(success, null);
    }   
}       
   
function carregarNoMapa(endereco) {
$('#txtchegada').val(endereco);  
var map;
var directionsDisplay; // Instanciaremos ele mais tarde, que será o nosso google.maps.DirectionsRenderer
var directionsService = new google.maps.DirectionsService();

directionsDisplay = new google.maps.DirectionsRenderer(); // Instanciando...
   var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);

   var options = {
      zoom: 15,
      center: latlng,
      icon: "images/cliente.png",
      mapTypeId: google.maps.MapTypeId.ROADMAP
   };

   map = new google.maps.Map(document.getElementById("map"), options);
   directionsDisplay.setMap(map); // Relacionamos o directionsDisplay com o mapa desejado

   // Se o navegador do usuário tem suporte ao Geolocation
  

  var enderecoPartida = $("#txtendereco").val();
  var enderecoChegada = endereco;
   
  
  var request = { // Novo objeto google.maps.DirectionsRequest, contendo:
      origin:enderecoPartida, // origem
      destination: enderecoChegada, // destino
      travelMode: google.maps.TravelMode.DRIVING // meio de transporte, nesse caso, de carro
   };
   
   directionsService.route(request, function(result, status) {
      if (status == google.maps.DirectionsStatus.OK) { // Se deu tudo certo
         directionsDisplay.setDirections(result); // Renderizamos no mapa o resultado
      }
   });
    
    
 CalculaDistancia(enderecoPartida,enderecoChegada);
}


function CalculaDistancia(end,cheg){
    tarifa();
  var service = new google.maps.DistanceMatrixService();
        service.getDistanceMatrix(
		{
		    origins: [end],
		    destinations: [cheg],
		    travelMode: google.maps.TravelMode.DRIVING
		}, callback);
    }
    function callback(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
             
            $('#lblkm').empty();
            $('#lbltime').empty();
            $('#lblvlr').empty();
            $('#vlrdesc').empty();
            var km = parseFloat(response.rows[0].elements[0].distance.text);
            var vl = parseFloat($("#tarifa").val());
            var tax = parseFloat($("#tx").val());
            var tim = parseFloat($("#tm").val());
                        
            var tm = parseFloat(response.rows[0].elements[0].duration.text);
            if (km > 30){
            var rs = tax + (km * vl)+(tm * tim);
            rs = rs * 1.2;
            }
            else{
            var rs = tax + (km * vl)+(tm * tim);   
            }
            $('#lblkm').val(response.rows[0].elements[0].distance.text+".");
            $('#lbltime').val(response.rows[0].elements[0].duration.text);
            $('#lblvlr').val(rs);
            $('#txtkm').val(response.rows[0].elements[0].distance.text+".");
            
            var indId = $("#lblpontos").val();
            var cDesc = rs - ((indId/100)*rs);
            $('#vlrdesc').val(cDesc);
        }
        var item="<li class='list-group-item'>Distância:"+response.rows[0].elements[0].distance.text+"</li><li class='list-group-item'>Tempo:"+response.rows[0].elements[0].duration.text+"</li><li class='list-group-item'>Preço:R$ "+rs+"</li><li class='list-group-item'>Preço com Desconto:R$ "+cDesc+"</li>";
        $('#ltcard').empty();
        $('#ltcard').append(item);
        $("#Tarifa1").modal("toggle"); 
  }


        
    function pesquisacep(valor) {

        
      var cep_code = valor;
      if( cep_code.length <= 0 ) return;
      $.get("http://apps.widenet.com.br/busca-cep/api/cep.json", { code: cep_code },
         function(result){
            if( result.status!=1 ){
               alert(result.message || "CEP não encontrado");
               return;
            }
            $("#txtuf").val( result.state );
            $("#txtcidade").val( result.city );
            $("#txtbairro").val( result.district );
            $("#txtrua").val( result.address );
           
         });
                
    
    }
    function carregarNoMapaD(endereco) {
    var latlng = new google.maps.LatLng(-18.8800397, -47.05878999999999);
    var options = {
        zoom: 5,
        center: latlng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
 
    map = new google.maps.Map(document.getElementById("map"), options);
 
    geocoder = new google.maps.Geocoder();
 
    marker = new google.maps.Marker({
        map: map,
        draggable: true,
    });
 
    marker.setPosition(latlng);
        geocoder.geocode({ 'address': endereco + ', Brasil', 'region': 'BR' }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var latitude = results[0].geometry.location.lat();
                    var longitude = results[0].geometry.location.lng();
 
                    $("#lat").val(latitude);
                    $("#lng").val(longitude);
 
                    var location = new google.maps.LatLng(latitude, longitude);
                    marker.setPosition(location);
                    map.setCenter(location);
                    map.setZoom(16);
                }
            }
        });
}
function login(use,sen){
    
    var ids ="";
    $("#msglog").empty();
    $("#msglog").append("Analisando.");
    $("#msglog").empty();
    $("#msglog").append("Analisando..");
    $("#msglog").empty();
    $("#msglog").append("Analisando...");
    
    $("#msglog").empty();
      
    $.ajax({
            
            type: "GET",
            url: "http://uber.factorysoftwares.com.br/wsdl.php/login?email="+use+"&senha="+sen,
            timeout: 3000,
            contentType: "application/json; charset=utf-8",
            
            beforeSend:function() {
            $("#msglog").append("Enviando...");
           var $progress = $('.progress');    
            var $progressBar = $('.progress-bar');    
           
                $progressBar.animate({width: "100%"}, 100);
                $progress.delay(1000).fadeOut(500);
            },
        
            success: function(request,status) {
            ids = JSON.parse(request);
            var id = ids.Id;
            var nome= ids.Nome;
            var pontos = ids.Pontos;
           
                  
                if (id > 0){
                 $("#idmor").empty();
                 $("#idmor").val(id);
                 $("#MyCod").empty();
                 $("#MyCod").val("Bem vindo seu código para indicação é: "+id);
                 $("#lblpontos").empty();
                 $("#lblpontos").val(pontos);
                 $("#txtnome").empty();
                 $("#txtnome").val("<h2 style='color:#FFF'>"+nome+"</h2>");
                 $("#txtuser").val(null);
                 $("#txtsenha").val(null);
                
                   
                 reaguarde();   
                 getMapLocation();    
                $("#inicio").modal("show");
                activate_page("#Home"); 
                

                document.getElementById("#lng").style.visibility = "hidden"; 
                document.getElementById("#lat").style.visibility = "hidden"; 
                     
                document.getElementById("#txtchegada").style.visibility = "hidden";  
                document.getElementById("#txtendereco").style.visibility = "hidden"; 
                document.getElementById("#idmor").style.visibility = "hidden";
             }else{
                $("#msglog").empty();
                $("#msglog").append("<h4>Erro no Login!<h4>");
                
             }
            },
            error:function(){
            ids = "Erro";
            },
           });
      
}


 function cadastros(us,mail,pass,fone,indica){
     
        var ids ="";
        
        $("#msgcadastro").empty();
        $.ajax({
        
        type: "GET",
            url: "http://uber.factorysoftwares.com.br/wsdl.php/cadastro?nome="+us+"&email="+mail+"&senha="+pass+"&fone="+fone+"&indica="+indica,
            timeout: 3000,
            contentType: "application/json; charset=utf-8",
            
          
        
            success: function(request,status) {
            ids = JSON.parse(request);
            var id = ids.Tipo;
            var nome= ids.Texto;
            
             if (id > 0){
            $("#txtname").val(null);
            $("#txtsenh").val(null);
            $("#txtconfsen").val(null);
            $("#txtemail").val(null);
            $("#txtfone").val(null);
                $("#msgcadastro").empty();
                $("#msgcadastro").append("<h4>Cadastro efetuado com sucesso.<h4>");
                 
             }else{
                $("#msgcadastro").empty();
                $("#msgcadastro").append("<h4>Já existe cadastro para este email.<h4>");
                
             }
            },
            error:function(){
            ids = "Erro";
            },
           });
    }

function agendar(nome,email,fone,saida,destino,hora){
     
        var ids ="";
        
        $("#msgcadastro1").empty();
        $.ajax({
        
        type: "GET",
            url: "http://uber.factorysoftwares.com.br/wsdl.php/agenda?us="+nome+"email="+email+"&fone="+fone+"&destino="+saida+"&senha="+destino+"&hora="+hora,
            timeout: 3000,
            contentType: "application/json; charset=utf-8",
            
          
        
            success: function(request,status) {
            ids = JSON.parse(request);
            var id = ids.Tipo;
            var nome= ids.Texto;
            
             if (id > 0){
            $("#nome1").val(null);     
            $("#email1").val(null);     
            $("#fone1").val(null);
            $("#saida1").val(null);
            $("#destino1").val(null);
            $("#hora1").val(null);
                $("#msgcadastro1").empty();
                $("#msgcadastro1").append("<h5>Obrigado em breve iremos em contato.<h5>");
                 
             }else{
                $("#msgcadastro1").empty();
                $("#msgcadastro1").append("<h4>Não foi possível agendar.<h4>");
                
             }
            },
            error:function(){
            ids = "Erro";
            },
           });
    }
function atualizar(us,mail,pass,fone,ende){
        $("#msgperfil").empty();
        var ids ="";
        
        $.ajax({
        
        type: "GET",
            url: "http://uber.factorysoftwares.com.br/wsdl.php/atualiza?nome="+us+"&email="+mail+"&senha="+pass+"&fone="+fone+"&end="+ende,
            timeout: 3000,
            contentType: "application/json; charset=utf-8",
            
            beforeSend:function() {
            $("#msgperfil").append("Enviando...");
            },
        
            success: function(request,status) {
            ids = JSON.parse(request);
            var id = ids.Tipo;
            var nome= ids.Texto;
            
            $("#txtnamep").val(null);
            $("#txtsenhp").val(null);
            
            $("#txtemailp").val(null);
            $("#txtfonep").val(null);
            $("#txtfonep").val(null);    
                $("#msgperfil").empty();
                $("#msgperfil").append("<h4>Dados atualizado com sucesso.<h4>");
                 
             
            },
            error:function(){
            ids = "Erro";
            },
           });
    }
function cadastrosm(us,mail,fone,mod,ano){
       
        var ids ="";
        
        $("#msgcadastrom").empty();
        $.ajax({
        
        type: "GET",
            url: "http://uber.factorysoftwares.com.br/wsdl.php/cadastromoto?nome="+us+"&email="+mail+"&fone="+fone+"&modelo="+mod+"&ano="+ano,
            timeout: 3000,
            contentType: "application/json; charset=utf-8",
            
                   
            success: function(request,status) {
            ids = JSON.parse(request);
            var id = ids.Tipo;
            var nome= ids.Texto;
            
            $("#txtnamem").val(null);
            $("#txtmodelo").val(null);
            $("#txtano").val(null);
            $("#txtemailm").val(null);
            $("#txtfonem").val(null);
            $("#msgcadastrom").append('<h4>Obrigado!Entraremos em contato em breve<h4>');           
                

            
             },
            
            error:function(){
            ids = "Erro";
            },
           });
    }


function chamar(cliente,saida,chega,pg,vlr,obs,pref){
        
        Latitude = $("#lat").val();
        
        Longitude = $("#lng").val();
       
        km = $("#txtkm").val();
        tm = $('#lbltime').val();
        var ids ="";
        //$("#ld").modal("show"); 
       
        $("#listchamado").empty();
        
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/chamar?Id="+cliente+"&saida="+saida+"&chega="+chega+"&pg="+pg+"&valor="+vlr+"&lat="+Latitude+"&lng="+Longitude+"&obs="+obs+"&km="+km+"&time="+tm+"&pref="+pref,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
            
            
            success: function(request,status) {
               
                aguarde();
                
            },
            
        
            error:function(){
            ids = "Erro";
            },
           });
    }

function aguarde(){
        
        $("#listchamado").empty();
        $("#fotos").empty();
        
        var ids ="";
        var cliente = $("#idmor").val();
        //$(".uib_w_53").modal("hide");
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/aguardo?cli="+cliente,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
            success: function(request,status) {
            ids = JSON.parse(request);
            $.each(ids,function(i, aviso){
            $("#idsts").empty();
            $("#idsts").append(aviso.Id);
                var $progress = $('.progress');    
                var $progressBar = $('.progress-bar');    
           
                $progressBar.animate({width: "100%"}, 10);
                $progress.delay(1000).fadeOut(500);
            
                var item= "<a class='list-group-item allow-badge widget uib_w_61' data-uib='twitter%20bootstrap/list_item' data-ver='1'><h4 class='list-group-item-heading'></h4><p class='list-group-item-text'><h4><p class='list-group-item-text'><b>Parabéns sua solicitação foi concluida, o motorista abaixo já esta a caminho.</b></p><p class='list-group-item-text'>Placa do Veiculo :"+aviso.PLACA+"</p><p class='list-group-item-text'>MODELO :"+aviso.MARCA+"</p><p class='list-group-item-text'>COR: "+aviso.COR+"</p><p class='list-group-item-text'>Nome do Motorista :"+aviso.MOTORISTA+"</p></h4></a><iframe width='150px' frameborder='0' style='text-align:center;' src=http://uber.factorysoftwares.com.br/fotos/"+aviso.Foto+"></iframe>";
                $("#txtidcor").empty();
                $("#txtidcor").val(aviso.Id);
                $("#listchamado").append(item);
                $("#corrida").modal("show"); 
                });
            $("#atencao").modal("hide");
            $("#corrida").modal("show");
             
            },
           
            error:function(){
            ids = "Erro";
            },
           });
    }
    
    function reaguarde(){
        
        $("#listchamado").empty();
        $("#fotos").empty();
    
        var ids ="";
        var cliente = $("#idmor").val();
        $(".uib_w_53").modal("hide");
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/reaguardo?cli="+cliente,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
                        

            success: function(request,status) {
            ids = JSON.parse(request); 
            $.each(ids,function(i, aviso){
            $("#idsts").empty();
            $("#idsts").append(aviso.Id);
               
            
            if(aviso.Id == '0'){
                
            $("#idsts").empty();         
            } 
            else{  
                $("#idsts").empty();
                $("#idsts").append(aviso.Id);
                //foto
         var item= "<a class='list-group-item allow-badge widget uib_w_61' data-uib='twitter%20bootstrap/list_item' data-ver='1'><h4 class='list-group-item-heading'></h4><p class='list-group-item-text'><h4><p class='list-group-item-text'>Parabéns sua solicitação foi concluida, o motorista abaixo já esta a caminho,é só aguardar.</p><p class='list-group-item-text'>Placa do Veiculo :"+aviso.PLACA+"</p><p class='list-group-item-text'>MODELO :"+aviso.MARCA+"</p><p class='list-group-item-text'>COR: "+aviso.COR+"</p><p class='list-group-item-text'>Motorista :"+aviso.MOTORISTA+"</p></h4></a><iframe width='150px' frameborder='0' style='text-align:center;'src=http://uber.factorysoftwares.com.br/fotos/"+aviso.Foto+"></iframe>";
                $("#txtidcor").empty();
                $("#txtidcor").val(aviso.Id);
                $("#listchamado").append(item);
                $("#fotos").append(foto);
                
                

            if(aviso.stscli == '0'){ 
                
            $("#corrida").modal("hide");
            }
            else{
                
            
            $("#corrida").modal("show"); 
                
            }
                
            }
            });
             },
           
            error:function(){
            ids = "Erro";
            },
           });
    }
    


    function chat(){
        
     var Id = $("#txtidcor").val();
        
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/chat?Id="+Id,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
           
            success: function(request,status) {
            $("#listdrive").empty();
            ids = JSON.parse(request); 
            $.each(ids,function(i, aviso){
            var hr = aviso.Hora;
            var car = aviso.Carro;
                if (car === null){
                    car=''; 
                    hr='';
                }
                
            var cli = aviso.Cliente;    
             if (cli === null){
                    cli=''; 
                    
                }
            var item= "<a class='list-group-item allow-badge widget uib_w_61' data-uib='twitter%20bootstrap/list_item' data-ver='1'><p style='text-align:left' class='list-group-item-text' ><b>Motorista:</b><br>"+car+"</p><p class='list-group-item-text' style='text-align:right'><b>Você:</b><br>"+cli+"</p></a>";
            $("#listdrive").append(item);   
           
            })
            
            },
            
            error:function(){
            ids = "Erro";
            },
           });
                   
            
           
}



    function msgc(){
         
        
    var Id = $("#txtidcor").val();
     var msg = $("#txtmsg").val();    
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/msgc?Id="+Id+"+&msg="+msg,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
           
            success: function(request,status) {
            ids = JSON.parse(request); 
             chat(); 
                var msg = $("#txtmsg").val(null); 
            },
            
            error:function(){
            ids = "Erro";
            },
           });
            
        
            
           
}


function tarifa(){
        
            $.ajax({
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/tarifa",
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
            ids = JSON.parse(request); 
           
            var vlr = ids.Id;
            var tx =  ids.Taxa;  
             var tm =  ids.Time; 
            $("#tarifa").val(vlr);
             
            $("#tx").val(tx);
            $("#tm").val(tm);
            
            
            
            },
            error:function(){
            ids = "Erro";
            },
            });
            }

function ac(){
        
        var ids ="";
        var idCor = $("#txtidcor").val();
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/aceita?cli="+idCor,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
            $("#corrida").modal("hide");  

            $.each(ids,function(i, aviso){
            });
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    }
function zera(){
        
        var ids ="";
        var idCor = $("#idmor").val();
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/zerabonus?cli="+idCor,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
           
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    }

function cancelar(){
        
        var ids ="";
        var idCor = $("#txtidcor").val();
        
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/cancelar?cli="+idCor,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
                alert("Sua corrida foi cancelada");
                $("#corrida").modal("hide");
           
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    }
function avaliar(){
        
        var ids ="";
        var idCor = $("#txtidcor").val();
        var va = $("#vote").val();

        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/avalia?cli="+idCor+"&avalia="+va,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
            alert("Obrigado pela avaliação");
           
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    }
function senha(){
        
        var ids ="";
        var idCor = $("#txtlembrar").val();
        var ret = validateEmail(idCor);
        if(!ret){
        alert('Email incorreto'); 
        return false;
        }
        $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/wsdl.php/senha?email="+idCor,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            success: function(request,status) {
            $("#poplembrar").modal("hide");
            $("#idalert").modal("show");

            $.each(ids,function(i, aviso){
            });
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    }
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  
  }
}


      
function validateEmail(email){  
                
                    er = /^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2}/;
                    if(er.exec(email))
                        return true;
                    else
                        return false;
                

}

function validarpg(){
    
    var idCor = $("#txtidcor").val();
    
     $.ajax({
            
            type: "GET",
            url:"http://uber.factorysoftwares.com.br/payemail.php/id="+idCor,
            timeout: 3000,
            async: false,
            contentType: "application/json; charset=utf-8",
            
           
            success: function(request,status) {
            

            $.each(ids,function(i, aviso){
            
            if(aviso.total == '0'){
                validarpg();
                     
            } 
            else{      
                var  deonde = $("#txtendereco").val(); 
                var paraonde = $("#txtchegada").val();
                var cliente = $("#idmor").val();
                var tipopg = $("#tipg").val();
                var vlr = $("#lblvlr").val(); 
                chamar(cliente,deonde,paraonde,tipopg,vlr);
            }
            });
            },
            
            error:function(){
            ids = "Erro";
            
            },
                
           });
    
}


 document.addEventListener("deviceready", onDeviceReady, false);